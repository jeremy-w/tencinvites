import bottle
from bottle.ext import beaker
import configparser
from collections import namedtuple
import datetime
import logging
import tenc

log = logging.getLogger(__name__)

Invite = namedtuple(
	'Invite',
	'url guid')


app = bottle.Bottle()
app.config.load_config('config.ini')
CONFIG_KEY_APP_TOKEN = 'tenc.app_token'


# http://beaker.readthedocs.io/en/latest/configuration.html#session-options
stack = beaker.middleware.SessionMiddleware(app, app.config)
SESSION_KEY_EMAIL = 'tenc.email'
SESSION_KEY_TOKEN = 'tenc.token'


@app.route('/login')
def show_login():
	return bottle.template(
		'login.html',
		api_error=''
		)


def session_dict_from_request(request):
	return request.environ.get('beaker.session')
	

@app.post('/login')
def create_login():
	app_token = app.config[CONFIG_KEY_APP_TOKEN]
	params = bottle.request.params
	email = params.get('email', default='')
	password = params.get('password', default='')
	if not email or not password:
		return bottle.template(
			'login.html',
			api_error='Both email and password must be provided.')
	try:
		user_token = tenc.login(
			app_token,
			email,
			password)
		session = session_dict_from_request(bottle.request)
		session[SESSION_KEY_EMAIL] = email
		session[SESSION_KEY_TOKEN] = user_token
		session.save()
		bottle.redirect(
			app.get_url('/'))
	except tenc.LoginError as e:
		return bottle.template(
			'login.html',
			api_error=str(e)
		)


@app.route('/')
def show_invites():
	session = session_dict_from_request(bottle.request)
	email = session.get(SESSION_KEY_EMAIL, None)
	token = session.get(SESSION_KEY_TOKEN, None)
	if not email or not token:
		log.info('msg="no session found, redirecting to /login" email={}'.format(email))
		bottle.redirect(
			app.get_url('/login'))
	
	raw_invites = tenc.invites(tenc.make_session(token))
	invites = [
		Invite(
			url='https://beta.10centuries.org/#' + i['guid'],
			guid=i['guid'])
		for i in raw_invites
		if 'redeemed' not in i
		]
	return bottle.template(
		'index',
		account=email,
		invites=invites
		)


def run_for_development():
	bottle.debug(True)
	bottle.run(
		stack,
		host='localhost',
		port=8080,
		reloader=False)  # ENOPERM on iOS


if __name__ == '__main__':
	logging.basicConfig(level=logging.DEBUG)
	run_for_development()
