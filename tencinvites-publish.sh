set -x
set -e

REPO="/home/jeremy/Pythonista/tencinvites.git"
CONFIG="/home/jeremy/tencinvites-config.ini"

# Update the app code
cd "$REPO"
git archive --format=tar --prefix=tencinvites/ HEAD \
  | (cd /srv/uwsgi/ && tar -xf -)
cp "$CONFIG" /srv/uwsgi/tencinvites/config.ini
chown -R http:http /srv/uwsgi/tencinvites/

# Update the UWSGI confi
install /srv/uwsgi/tencinvites/tencinvites.ini /etc/uwsgi/tencinvites.ini
systemctl restart uwsgi@tencinvites.service
