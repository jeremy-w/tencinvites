# This is a command-line tool using the tenc library.
# You'll need to fill in your user token,
# since it doesn't do any auth itself.
import pprint
import tenc

USER_TOKEN = 'YOUR TOKEN HERE'


def report_invites(session):
	'''
	Prints the invite count and lists any not-yet-redeemed invites.
	'''
	invites = tenc.invites(session)
	shareable = [
		invite['guid']
		for invite in invites
		if 'redeemed' not in invite
		]
	print(
		'{} not yet redeemed of {} invites total:\n'.format(len(shareable), len(invites)))
	invite_links = [
		'https://beta.10centuries.org/#' + guid
		for guid in shareable
		]
	print('\n\n'.join(invite_links))


if __name__ == '__main__':
	session = tenc.make_session(USER_TOKEN)
	report_invites(session)
