<!doctype html>
<html class="no-js" lang="en-US">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>10C Mobile Invite Viewer</title>
        <meta name="description" content="View your 10Centuries.org invites in a form easily shareable from mobile devices.">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

        {{!base}}
        
        <footer style='margin-top: 24pt; border-top: 1px solid; font-size: 80%;'>
        <p>Painstakingly assembled in Pythonista on an iPhone 6 Plus by Jeremy W. Sherman in March 2017.</p>
        <p>Mention <strong>@jws</strong> on 10Centuries with any questions or feedback.</p>
        </footer>
    </body>
</html>
