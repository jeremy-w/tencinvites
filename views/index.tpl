% rebase('page.tpl')
<p>Hello, <strong>{{account}}</strong>!</p>
<p>You have <strong>{{len(invites)}}</strong> unredeemed {{'invites' if len(invites) != 1 else 'invite'}}:
<ol>
% for invite in invites:
  <li><a href="{{invite.url}}">{{invite.guid}}</a></li>
% end
</ol>
</p>
