# This is an abandoned start at a secure cookie API roughly per Liu et al. 2005.
# I'd back out a good chunk of it for cryptography's Fernet if I were keeping 
# on with it, I think.
import base64
from collections import namedtuple

Session = namedtuple(
	'Session',
	'email tenc_token')
Cookie = namedtuple(
	'Cookie',
	'expiry_unix email json')
SecureCookie = namedtuple(
	'SecureCookie',
	'expiry_unix email encdata hmac')


def parse_secure_cookie(c):
	'''
	Assumes cookie looks like:
		unix emailb64 b64 hmacb64
	'''
	chunks = c.split(' ')
	if len(chunks) != 4:
		log.warn('msg="cookie has invalid format" cookie="{}"'.format(c))
		return None
	unix, emailb64, datab64, hmacb64 = chunks
	try:
		cookie = SecureCookie(
			expiry_unix=unix,
			email=base64.b64decode(emailb64),
			encdata=base64.b64decode(datab64),
			hmac=base64.b64decode(hmacb64)
			)
		log.debug('msg="successfully parsed session cookie as secure cookie"')
		return cookie
	except base64.binascii.Error:
		log.exception('failed to base64 decode a cookie chunk')
		return None


def sign_secure_cookie(secure_cookie,  secret):
	# TODO: sign
	return secure_cookie


def encrypt_cookie(cookie, secret):
	data = base64.encode(json.dumps(cookie.json, sort_keys=True))
	# TODO: encrypt data
	return SecureCookie(
		expiry_unix=cookie.expiry_unix,
		email=cookie.email,
		encdata=data,
		hmac=None)


def decrypt_secure_cookie(secure_cookie, secret):
	jdict = json.loads(base64.b64decode(secure_cookie.encdata))
	return Cookie(
		expiry_unix=secure_cookie.expiry_unix,
		email=secure_cookie.email,
		json=jdict)


def session_from_request(request):
	'''Returns a Session if any, else None.'''
	raw_cookie = request.get_cookie('session')
	if not raw_cookie:
		return None
	
	secure_cookie = parse_secure_cookie(raw_cookie)
	if not secure_cookie:
		return None

	ttl = secure_cookie.expiry_unix - datetime.datetime.utcnow().timestamp()
	is_expired = ttl <= 0
	if is_expired:
		log.debug('msg="cookie expired", seconds_ago={}'.format(round(ttl)))
		return None
	
	secret = request.app.config['tenc.secret']
	comparison = sign_secure_cookie(secure_cookie,  secret)
	if comparison.hmac != secure_cookie.hmac:
		log.warn('msg="corrupt hmac received for secure cookie"')
		return None
	
	cookie = decrypt_secure_cookie(secure_cookie, )
	if not cookie:
		return None
	token = cookie.json['tenc.user_token']
	session = Session(
		email=cookie.email,
		tenc_token=token)
	return session
	

def save_session(session):
	pass

