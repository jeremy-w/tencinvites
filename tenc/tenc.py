import attr
import enum
import pprint
import requests
import logging
log = logging.getLogger(__name__)


def make_session(user_token):
	s = requests.session()
	s.headers = {
		'Authorization': user_token,
	}
	return s
	
	
def _url(path):
	return 'https://api.10centuries.org' + path


class Error(Exception):
	pass 


class LoginError(Error):
	def __init__(self,email, message, json):
		self.email = email 
		self.message = message
		self.json = json
		
	def __str__(self):
		return 'Failed to log in as {}: {}'.format(self.email, self.message)
	

def login(app_token, email, password):
	'''
	Log a user in. If successful, returns a user token string.
	If failed, throws a LoginError.
	
	See: https://docs.10centuries.org/auth#login
	'''
	data = {
		'acctname': email,
		'acctpass': password,
		'client_guid': app_token,
	}
	r = requests.post(_url('/auth/login'), data=data)
	json = r.json()
	info = json['data']
	if info:
		token = info.get('token', None)
		if token:
			return token
	
	message = json['meta'].get('text', 'No error message was provided by 10C.')
	raise LoginError(email, message, json)


def invites(session):
	'''
	Returns all the user's invites and their status.
	
	Returns: [{guid, created, redeemed: {by, at}?}]
	'''
	# {'meta': {'code': 200, 'server': '23.65'}, 'data': [{'created_unix': 1476585292, 'redeemed_at': False, 'redeemed_unix': False, 'created_at': '2016-10-16T02:34:52Z', 'redeemed_by': False, 'guid': '7d919dfb3435c3bbf72726a0c2b9facbf7c87a52b2dcab066e39599cb7925553', 'created_by': {'id': 27}}, {'created_unix': 1476585292, 'redeemed_at': '2017-02-11 22:34:19', 'redeemed_unix': 1486852459, 'created_at': '2016-10-16T02:34:52Z', 'redeemed_by': {'id': 143}, 'guid': '52cad5a049baf9388ea007b11b6fb908c22b93e1d4a3cb76cf85d82476d056a2', 'created_by': {'id': 27}}]}
	r = session.get(_url('/invite'))
	invites = r.json()['data']
	if not invites:
		return []
	
	out = []
	for i in invites:
		guid = i['guid']
		# created = datetime.datetime.utcfromtimestamp(i['created_unix'])
		created = i['created_at']
		
		redeemed = {}
		info = i['redeemed_by']
		if info:
			# redeemed['at'] = datetime.datetime.utcfromtimestamp(i['redeemed_unix'])
			redeemed['at'] = i['redeemed_at']
			redeemed['by'] = info['id']
			
		one = {
			'guid': guid,
			'created': created,
		}
		if redeemed:
			one['redeemed'] = redeemed
		out.append(one)
	return out


GLOBAL_FILTER = enum.Enum('GLOBAL_FILTER', 'global home mentions interactions stars pins')


POST_BOUND_KIND = enum.Enum('POST_BOUND_KIND', 'unix post_id')


@attr.s
class PostBound:
	value = attr.ib(convert=int)
	kind = attr.ib(default=POST_BOUND_KIND.post_id)



def global_posts(
	session,
	filter=GLOBAL_FILTER.mentions,
	count=20,
	before=None,
	since=None,
	simple=True):
	data = {}
	if count:
		data['count'] = count
	if before:
		data['before_unix'] = before
	if since:
		data['since_unix'] = since
	if simple:
		data['simple_posts'] = 'Y'
	r = session.get(_url('/content/blurbs/' + filter.name), data=data)
	return r
	
	
	
if __name__ == '__main__':
	if False:
		s = make_session()
		r = global_posts(
			s,
			filter=GLOBAL_FILTER.stars,
			count=10,
			simple=False)
		pprint.pprint(r)
