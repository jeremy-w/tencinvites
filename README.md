# tencinvites
This is a small web site that makes it easy to share invite links from a mobile phone.

## Why
It was developed because the 10C Admin interface makes it impossible to see which invites are as yet unused on a mobile phone due to truncation.

Additionally, it's annoying to need to manually build the URL to share to pre-fill the token for the invitee.

The web app fixes both these problems by showing only unused invites and making it easy to copy the full URL.

## How
The app was developed using Pythonista for Python 3. StaSH was used to pull in the bottle_beaker plugin via pip install. You might need to play around a bit to get it up and running in a venv.

## Deploying
You'll need to copy config.ini.example to config.ini and fill it out. If you run app.py directly, you should be able to reach the app and use it then.

As I deployed it, there ended up being three moving parts:

- uwsgi: runs the app code via its Python plugin and handles requests sent in by nginx
- nginx: receives inbound requests and routes certain ones to uwsgi
- systemd: runs nginx and the uwsgi app

For Nginx, I added a simple location stanza in the HTTPS server stanza to send requests with a certain prefix to the uwsgi app:

```
location /tencinvites {
  include uwsgi_params;
  uwsgi_pass unix:/run/uwsgi/tencinvites.socket;
}
```

The rest of the setup is handled in my case by the tencinvites-publish.sh script, so have a look-see there for how uwsgi and systemd come together.

Before you run that though, you will want to **verify you can run app.py directly**; it turns out, if a module fails to load, uwsgi just goes into full dynamic mode rather than failing, which can make things look like they're working from the point of view of `systemctl status` when they're actually not.

## License 
The app code is licensed under the Mozilla Public License, version 2.
